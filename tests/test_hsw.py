# !/usr/bin/env python
# -*- coding: utf8 -*-

import os
import unittest
from pathlib import Path

from highscoringwords import HighScoringWords


class TestScores(unittest.TestCase):
    def setUp(self):
        # Supplied long word list
        self.wordlist = HighScoringWords(os.path.join(Path(__file__).parent.parent, 'wordlist.txt'))

        # Test word list
        self.test_words = HighScoringWords(os.path.join(Path(__file__).parent.parent, 'test_words.txt'))

        # Leaderboard for words
        self.leaderboard = self.wordlist.build_leaderboard_for_word_list()

    def test_word_values(self):
        """
        Test score of words in test wordlist supplied
        """
        self.test_words.build_leaderboard_for_word_list()
        self.assertEqual(self.test_words.word_scores['hypothesize'], 31)
        self.assertEqual(self.test_words.word_scores['dignitary'], 14)

    def test_leaderboard(self):
        """
        Test leaderboard list with supplied words in test words file
        """
        highest = self.test_words.build_leaderboard_for_word_list()
        self.assertEqual(highest[0], 'hypothesize')
        self.assertEqual(highest[10], 'lively')
        self.assertEqual(highest[-1], 'aa')

    def test_top_100(self):
        """
        Test various values of the top 100 leaderboard
        """
        self.assertEqual(self.leaderboard[0], 'razzamatazzes')
        self.assertEqual(self.leaderboard[50], 'psychopharmacology')
        self.assertEqual(self.leaderboard[-1], 'cyclohexylamines')

    def test_start_string(self):
        first_5 = self.test_words.build_leaderboard_for_letters('hos')
        micro_l = self.wordlist.build_leaderboard_for_letters('micro')
        anti_l = self.wordlist.build_leaderboard_for_letters('anti')
        self.assertEqual(first_5, ['hospitality'])
        self.assertEqual(micro_l[0], 'microelectrophoretically')
        self.assertEqual(anti_l[0], 'antischizophrenic')
        self.assertEqual(anti_l[-1], 'antiromanticisms')


if __name__ == '__main__':
    unittest.main()
