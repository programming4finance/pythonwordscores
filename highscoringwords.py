# !/usr/bin/env python
# -*- coding: utf8 -*-
"""
For this challenge, of the files provided to you, ONLY those listed here are editable.

    PythonWordScores/__init__.py
    PythonWordScores/highscoringwords.py

You can add files to your submission, but if you edit any of the provided files, other than those specified, your changes will be IGNORED.
Please read these instructions carefully before starting to code:

    When you assemble your code challenge solution for submission please create it as a ZIP file.
    Please do NOT include generated files in your submitted solution.
    Your ZIP upload will be cleaned to remove ignored (e.g. generated) files.
    Files will be removed and ignored based on standard gitignore files as specified in this GitHub project: github/gitignore.
    When you upload your code, you will be shown which files are accepted for code review and which will be ignored.
    The ignored files will NOT be provided to the reviewer.
    Create your solution WITHIN the structure of the starting ZIP file that you have downloaded.
    When you are ready to upload your code, you can re-ZIP from the starting top-level directory and upload the entire ZIP by clicking the Upload Solution button below.

This information is also included in the __init__.py file supplied in the skeleton code for the challenge. You can download the skeleton code and all the files you will need to complete the challenge by clicking the Download button.

Scrabble™ is a long-established and popular word game in many different languages.The object of the game is to build valid words (for this exercise words are valid if they are present in the wordlist.txt file supplied) from a set of letter (tiles) that the player holds.

Each letter carries a different score value based on its frequency in the language. For example in English vowels such as A and E score only 1 point but less frequent letters such as K and J score 5 and 8 points respectively. The score for any particular word is the sum of the values of all the letters that make up the word. So for example:the word cabbage scores C3 + A1 + B3 + B3 + A1 + G2 + E1 = 14 points. The score values of letters in English are shown in the letterValues.txt file (also supplied).

The objective of this coding challenge is twofold:

    to create a leaderboard of the 100 highest scoring words in English based on the words in the wordlist.txt file. Words should be ordered in descending order with the highest scoring first. If several words have the same score they should be ordered alphabetically.

    to create a leaderboard of the valid words that can be created from a supplied String of random letters. For example for the random String deora, some of the valid words are: road; read; and adore. The length of the random String may vary but can be assumed to be in the range of 5-15 characters. Again, words should be ordered in descending order with the highest scoring first. If several words have the same score they should be ordered alphabetically.

Some skeleton Python code is provided for the exercise. Please do not change the supplied code, but create additional code as required. A HighScoringWords class with two unimplemented functions matching the numbered objectives above is in the file highscoringwords.py

    def build_leaderboard_for_word_list(self):

    def build_leaderboard_for_letters(self, starting_letters):

Add your code to the highscoringwords.py file but do NOT change or remove the provided code. Although writing code to assemble the correct leaderboards is the highest priority, your code may also be benchmarked for performance.

YOUR CODE SHOULD THEREFORE BE OF PRODUCTION QUALITY
"""
__author__ = 'codesse'

import operator
import os
from typing import Dict, List

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


class HighScoringWords:
    CHART_LENGTH = 100
    MIN_WORD_LENGTH = 3
    letter_values = {}

    def __init__(self, validwords=os.path.join(ROOT_DIR, 'wordlist.txt'),
                 lettervalues=os.path.join(ROOT_DIR, 'letterValues.txt')):
        """
        Initialise the class with complete set of valid words and letter values
        by parsing text files containing the data
        :param validwords: a text file containing the complete set of valid
        words, one word per line
        :param lettervalues: a text file containing the score for each letter in
        the format letter:score one per line
        :return:
        """
        self.leaderboard = []
        self.word_scores = {}

        with open(validwords) as file:
            self.valid_words = file.read().splitlines()

        with open(lettervalues) as file:
            for line in file:
                key, val = line.split(':')
                self.letter_values[str(key).strip().lower()] = int(val)

    def build_leaderboard_for_word_list(self):
        """
        Build a leaderboard of the top scoring
        CHART_LENGTH words from the complete set of valid words.
        :return:
        """
        self.create_dict(self.valid_words, self.word_scores)
        res = sorted(self.word_scores.items(), key=operator.itemgetter(1), reverse=True)

        self.leaderboard = [i[0] for i in res][:self.CHART_LENGTH]
        return self.leaderboard

    def build_leaderboard_for_letters(self, letters: str):
        """
        Build a leaderboard of the top scoring MAX_LEADERBOAD_LENGTH words from the complete set of valid words.
        :param letters: random string of letters
        :return:
        """
        # Words list to append to
        words_ = []

        # Output words dict
        output_words = {}
        for word in self.valid_words:
            if word[:len(letters)] == letters:
                words_.append(word)
                self.create_dict(words_, output_words)

        # Per the instructions Words should be ordered in descending order with the highest scoring first.
        # If several words have the same score they should be ordered alphabetically.
        res = sorted(output_words.items(), key=operator.itemgetter(1), reverse=True)

        return [i[0] for i in res][:self.CHART_LENGTH]

    def create_dict(self, valid_words: List[str], dictionary: Dict):
        """
        Creates a dictionary containing all words that are valid and their scores.
        """
        for word in valid_words:
            dictionary[word] = 0
            for letter in word:
                dictionary[word] += self.letter_values[letter]
